var mongoose = require("mongoose");

var postSchema = new mongoose.Schema({
    code:String,
    subject:String,
    title:String,
    created:  {type: Date, default: Date.now}
});

module.exports = mongoose.model("post",postSchema);